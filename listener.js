"use strict";

var last_target = null;
var frames = [];

function subclassElements(tag)
{
  frames = [];
  var imgs = getCollection(tag);
  for(var i = 0; i < imgs.length; i++)
  {
    imgs[i].onmousedown = down;
  }
}

function contains(a, obj)
{
  var i = a.length;
  while(i--)
  {
    if(a[i] === obj)
    {
      return true;
    }
  }
  return false;
}

function getCollection(tagname, d)
{
  var d = typeof d !== 'undefined' ? d : document;
  
  frames.push(d.URL);
  
  var coll = [], oIframe, IFrameDoc, el, i = 0, j;
  
  //get elements in parent document
  var subcoll = d.getElementsByTagName(tagname);

  while(el = subcoll.item(i++))
  {
    coll[coll.length] = el;
  }
  
  // get elements in iframe documents (moved to manifest)
  /*
  var all_IFrames = d.getElementsByTagName('iframe');
  i = 0;
  while(oIframe = all_IFrames.item(i++))
  {
    try
    {
      IFrameDoc = oIframe.contentDocument;
    }
    catch(err) // some frames can have an origin in another extension
    {
      console.log("Frame skipped: " + err.name + " " + err.message);
      IFrameDoc = null;
    }
    if(!(IFrameDoc instanceof HTMLDocument)) continue; // can be external unaccessible frame 

    subcoll = getCollection(tagname, IFrameDoc); // recursion for frames in frames etc.
    j = 0;

    while(el = subcoll[j++])
    {
      coll[coll.length] = el;
    }
  }
  */
  return coll;
}


function installHandler()
{
  document.body.onmousedown = down;
  subclassElements("img");
  subclassElements("object");
  subclassElements("embed");
  // document.addEventListener('mousedown', down, true);
  console.log('Hide Elements installed in ' + document.location);
  chrome.extension.sendRequest({install: true});
}

function collectParents(element)
{
  var result = [];
  var startsWith = function(string, prefix)
  {
    return string.indexOf(prefix) === 0;
  }
 
  do
  {
    if(element.src || element.href || element.URL)
    {
      result.push(element.src || element.href || element.URL);
    }
    if(element.style && element.style.background && startsWith(element.style.background, "url("))
    {
      var str = element.style.background;
      result.push(str.substring(str.indexOf("(") + 1, str.indexOf(")")));
    }
  }
  while(!!(element = element.parentNode));
  return result;
}

function down(event)
{
  if(event.button == 2)
  {
    console.log('target:' + event.target.tagName + ' ' + event.target.src + ' ' + event.target.href);
    last_target = event.target;
    // last_target.style.backgroundColor = "yellow";
    var r = collectParents(event.target);
    
    chrome.extension.sendRequest({target: event.target.tagName, stack: r});
    
    // for objects we just check that the mouse button == 2
    if(event.target.tagName.toLowerCase() == "embed")
    {
      hideElement();
    }
  }
}

function hideElement()
{
  if(last_target) last_target.style.display = 'none';
  var selObj = document.getSelection();
  if(selObj)
  {
    selObj.deleteFromDocument();
  }
}

function addLoadEvent(func)
{ 
  var oldonload = window.onload; 
  if(typeof window.onload != 'function')
  { 
    window.onload = func; 
  }
  else
  { 
    window.onload = function()
    { 
      func(); 
      if(oldonload)
      { 
        oldonload(); 
      } 
    }
  } 
} 

addLoadEvent(installHandler);
