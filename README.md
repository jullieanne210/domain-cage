# README #

Domain Cage is a Chrome extension started back in 2008 as a completely new project inspired by eponymous Firefox extension. The sources are ready to install and run in Chrome using `Settings` -> `Extensions ` -> `Load unpacked extension` button. The packed version (signed `.crx` file) for everyday use can be obtained from the [Downloads](https://bitbucket.org/weborienteer/domain-cage/downloads) section, or from the [external site](http://weborienteer.com/domain-cage.crx) (direct link). Downloads may have a bit outdated version in comparison to the sources.


### Quick summary ###

Domain Cage can block web requests to all external domains except for the cases, when originating site or target site is white-listed. Also requests to black-listed URLs can be blocked.

Please find details about setup and usage in the [Wiki](https://bitbucket.org/weborienteer/domain-cage/wiki/Home).

### Installation ###

Copy contents of the repository to any place on your local disk, then `Load unpacked extension` in Chrome from the chosen place.

The code is self-contained, there are no external dependencies.

### Version ###

The latest version is 0.7.1.