"use strict";

var counterTo = {};

function readStorage()
{
  var a = chrome.extension.getBackgroundPage().blackList;
  for(var i = 0; i < a.length; i++)
  {
    add2TableTo(a[i], 'removeTo', counterTo);
  }
}

function removeTo()
{
  chrome.extension.getBackgroundPage().replaceBlackList(remove('main1', 'c_'));
  document.getElementById('removeTo').disabled = true;
}

document.addEventListener('DOMContentLoaded',
function()
{
  readStorage();
  document.getElementById('removeTo').addEventListener('click', removeTo);
});
