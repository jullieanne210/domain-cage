"use strict";

var tabs = {};
var blocked = [];
var whitelisted = [];

var whiteListTo = [];
var whiteListFrom = [];

var blackList = [];
var prefix = "Hide Element";
var cmdBlock = "Block URLs";

var chromeExtTab = "chrome://extensions/";
var debugLog = false;

// storage stuff

function isStorageSupported()
{
  try
  {
    return 'localStorage' in window && window['localStorage'] !== null;
  }
  catch (e)
  {
    return false;
  }
}

function setData(name, x)
{
  if(isStorageSupported())
  {
    localStorage[name] = x;
  }
}

function getData(name)
{
  if(isStorageSupported())
  {
    return localStorage[name]; 
  }
  return null;
}

function restoreWhiteListTo()
{
  var s = getData("whiteListTo");
  if(s != undefined && s.length > 0)
  {
    log('WhiteListTo read:' + s);
    whiteListTo = s.split(",");
  }
}

function restoreWhiteListFrom()
{
  var s = getData("whiteListFrom");
  if(s != undefined && s.length > 0)
  {
    log('WhiteListFrom read:' + s);
    whiteListFrom = s.split(",");
  }
}

function mergeWhiteListTo(add)
{
  for(var i = 0; i < add.length; i++)
  {
    if(whiteListTo.indexOf(add[i]) == -1)
    {
      whiteListTo.push(add[i]);
    }
  }
  var s = whiteListTo.join();
  log('WhiteListTo saved:' + s);
  setData("whiteListTo", s);
}

function mergeWhiteListFrom(add)
{
  for(var i = 0; i < add.length; i++)
  {
    if(whiteListFrom.indexOf(add[i]) == -1)
    {
      whiteListFrom.push(add[i]);
    }
  }
  var s = whiteListFrom.join();
  log('WhiteListFrom saved:' + s);
  setData("whiteListFrom", s);
}

function replaceWhiteListTo(add)
{
  whiteListTo = [];
  whiteListTo = add.slice(0);
  var s = whiteListTo.join();
  log('WhiteListTo saved:' + s);
  setData("whiteListTo", s);
}

function replaceWhiteListFrom(add)
{
  whiteListFrom = [];
  whiteListFrom = add.slice(0);
  var s = whiteListFrom.join();
  log('WhiteListFrom saved:' + s);
  setData("whiteListFrom", s);
}

// URL stuff

function URL(url)
{
  url = url || "";
  this.parse(url);
}

URL.prototype =
{
  href: "",
  protocol: "",
  host: "",
  hostname: "",
  port: "",
  pathname: "",
  search: "",
  hash: "",
    
  parse: function(url)
  {
    url = url || this.href;
    var pattern = "^(([^:/\\?#]+):)?(//(([^:/\\?#]*)(?::([^/\\?#]*))?))?([^\\?#]*)(\\?([^#]*))?(#(.*))?$";
    var rx = new RegExp(pattern); 
    var parts = rx.exec(url);
    
    this.href = parts[0] || "";
    this.protocol = parts[2] || "";
    this.host = parts[4] || "";
    this.hostname = parts[5] || "";
    this.port = parts[6] || "";
    this.pathname = parts[7] || "/";
    this.search = parts[8] || "";
    this.hash = parts[10] || "";
    
    this.update();
  },
    
  update: function()
  {
    this.host = this.hostname + (("" + this.port) ? ":" + this.port : "");
    this.href = this.protocol + '//' + this.host + this.pathname + this.search + this.hash;
  },
    
  assign: function(url)
  {
    this.parse(url);
  },
  
  similar: function(that)
  {
    if(that instanceof Object && typeof that.hostname == 'string')
    {
      var domains1 = this.hostname.split('.').reverse();
      var domains2 = that.hostname.split('.').reverse();
      for(var i = 0; i < 2; i++) // domains1.length
      {
        // if(domains1[i] == 'www' && i == domains1.length - 1) break;
        if(domains1[i] != domains2[i]) return false;
      }
      return true;
    }
    return false;
  }
}

// BLOCKING

function addBlocked(tabId, url)
{
  if(blocked[tabId] == undefined)
  {
    blocked[tabId] = {"origin": tabs[tabId], "urls":[], "domains":[]};
  }
  blocked[tabId].urls.push(url);
}

function interceptRequest(request)
{
  if(request && request.url)
  {
    if(request.tabId != -1 && request.type == "main_frame")
    {
      tabs[request.tabId] = request.url;
    }
    if(!tabs[request.tabId]) return {cancel: false};
    
    var urlCurrent = new URL(tabs[request.tabId]);
    var urlNext = new URL(request.url);
    
    if(request.type == "main_frame") // new page/site is loading in main window
    {
      if(debugLog) console.log('main_frame:' + request.tabId + ' ' + request.url);

      if(blocked[request.tabId]) delete blocked[request.tabId];
      if(whitelisted[request.tabId]) delete whitelisted[request.tabId];
      
      // icon will be automatically reset to default on every new page
      
      chrome.browserAction.setBadgeText({text:'', tabId:request.tabId});
      
      if(whiteListFrom.indexOf(urlCurrent.host) != -1)
      {
        //log('skip host:' + urlCurrent.host);
        whitelisted[request.tabId] = true;
        chrome.browserAction.setIcon({path:"domaincage_icon_green_32.png", tabId:request.tabId});
        return {cancel: false};
      }

      return {cancel: false};
    }
    
    // can be chrome: or chrome-extension:, ftp:, etc
    if(urlCurrent.protocol != 'http' && urlCurrent.protocol != 'https') return {cancel: false}; 
    if(urlNext.protocol != 'http' && urlNext.protocol != 'https' && urlNext.protocol != '') return {cancel: false};

    if(ifExist(request.url))
    {
      if(debugLog) console.log('blocked ' + request.url + ' by blacklist');
      addBlocked(request.tabId, "*\t" + request.url);
      if(request.type == "sub_frame" || request.type == "other")
      {
        if(debugLog) console.log('placeholder ' + request.url + ' ' + request.type);
        return {redirectUrl: chrome.extension.getURL("empty.html") + '?' + request.url};
      }
      else if(request.type == "image")
      {
        if(debugLog) console.log('placeholder ' + request.url + ' ' + request.type);
        return {redirectUrl: chrome.extension.getURL("empty.svg") + '?' + request.url};
      }
      else
      {
        return {cancel: true};
      }
    }
    
    if(whiteListTo.indexOf(urlNext.host) != -1)
    {
      return {cancel: false};
    } 

    if(whiteListFrom.indexOf(urlCurrent.host) != -1)
    {
      return {cancel: false};
    }

    var filtered = whiteListTo.filter(endsWith('.' + urlNext.host));
    if(filtered.length > 0)
    {
      return {cancel: false};
    }

    if(urlCurrent.similar(urlNext))
    {
      return {cancel: false};
    }    
    
    if(urlCurrent.host != urlNext.host)
    {
      if(debugLog) console.log('blocked ' + request.url + ' from ' + tabs[request.tabId] + ' ' + request.type);
      addBlocked(request.tabId, request.url);
      
      var domain = urlNext.host; // .replace(/^www\./g, "");
      
      if(blocked[request.tabId].domains.indexOf(domain) == -1)
      {
        blocked[request.tabId].domains.push(domain);
      }
      chrome.browserAction.setIcon({path:"domaincage_icon_red_32.png", tabId:request.tabId});
      
      chrome.browserAction.setBadgeText({text:blocked[request.tabId].domains.length.toString(), tabId:request.tabId});
      if(request.type == "sub_frame" || request.type == "other")
      {
        if(debugLog) console.log('placeholder ' + request.url + ' ' + request.type);
        return {redirectUrl: chrome.extension.getURL("empty.html") + '?' + request.url};
      }
      else if(request.type == "image")
      {
        if(debugLog) console.log('placeholder ' + request.url + ' ' + request.type);
        return {redirectUrl: chrome.extension.getURL("empty.svg") + '?' + request.url};
      }
      else
      {
        return {cancel: true};
      }
    }
  }
  return {cancel: false};
}

function endsWith(str)
{
  return function(suffix)
  {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
  }
}

function getTabHost(id)
{
  if(blocked[id])
  {
    var u = new URL(blocked[id].origin);
    return u.host;
  }
  return null;
}

function tabCreated(tab)
{
  if(tab.url != undefined && tab.url != null)
  {
    if(tab.url == chromeExtTab)
    {
      if(!debugLog)
      {
        log('logging enabled');
        debugLog = true;
      }
    }
    if(debugLog) console.log('created:' + tab.id + ' ' + tab.url);
    tabs[tab.id] = tab.url;
  }
}

function tabUpdated(tabId, changeInfo, tab)
{
  // if changeInfo.url is presented, url has been updated,
  // but in some cases tab.url is filled without this 
  if(tab.url) // it's equal to changeInfo.url
  {
    if(tab.url == chromeExtTab)
    {
      if(!debugLog)
      {
        log('logging enabled');
        debugLog = true;
      }
    }
    if(debugLog) console.log('updated:' + tabId + ' n:' + tab.url + ' o:' + tabs[tabId] + ' ' + blocked[tabId] + ' ' + whitelisted[tabId]);
    if(!tabs[tabId])
    {
      tabs[tabId] = tab.url;
    }
    
    // this event does not fire all the time
    if(blocked[tabId] != undefined)
    {
      chrome.browserAction.setIcon({path:"domaincage_icon_red_32.png", tabId:tabId});
      chrome.browserAction.setBadgeText({text:blocked[tabId].domains.length.toString(), tabId:tabId});
    }
    else
    if(whitelisted[tabId] != undefined)
    {
      chrome.browserAction.setIcon({path:"domaincage_icon_green_32.png", tabId:tabId});
    }
  }
}

function log(s)
{
  console.log(s);
}

restoreWhiteListTo();
restoreWhiteListFrom();

chrome.tabs.onCreated.addListener(tabCreated);
chrome.tabs.onUpdated.addListener(tabUpdated);
chrome.webRequest.onBeforeRequest.addListener(interceptRequest, {urls: ["*://*/*"]}, ['blocking']);


// Hide Elements

function restoreBlackList()
{
  var s = getData("blackList");
  if(s != undefined && s.length > 0)
  {
    log('BlackList read:' + s);
    blackList = s.split('\t');
  }
}

function mergeBlackList(add)
{
  if(blackList.indexOf(add) == -1)
  {
    blackList.push(add);
    var s = blackList.join('\t');
    setData("blackList", s);
    log('BlackList saved:' + s);
  }
}

function replaceBlackList(add)
{
  blackList = [];
  blackList = add.slice(0);
  var s = blackList.join('\t');
  log('BlackList saved:' + s);
  setData("blackList", s);
}

function urlOffset(v) 
{
  for(var i = 0; i < blackList.length; i++)
  {
    if(v.indexOf(blackList[i]) != -1)
    {
      return i;
    }
  }
  return -1;
}

function ifExist(v)
{
  return urlOffset(v) > -1; 
}

var mid = chrome.contextMenus.create(
{
  contexts: ['all'], // ['image', 'link' , 'frame'], 
  title: prefix,
  onclick: function(info, tab)
  {
    chrome.tabs.executeScript(tab.id, {code: 'hideElement();'});
  }
});

var bid = chrome.contextMenus.create(
{
  contexts: ['all'],
  title: cmdBlock
});
  
var ready2blockIds = [];

function createMenuItem(creationObject, onclickHandler, callback)
{
  if(onclickHandler)
  {
    creationObject.onclick = function(onClickData, tab)
    {
      onclickHandler(onClickData, tab, creationObject);
    };
  }
  return chrome.contextMenus.create(creationObject, callback);
}

function removeOldUrls()
{
  for(var i = 0; i < ready2blockIds.length; i++)
  {
    chrome.contextMenus.remove(ready2blockIds[i]);    
  }
  ready2blockIds = [];
}

chrome.extension.onRequest.addListener(
function(request, sender, callback)
{
  if(request.target)
  {
    chrome.contextMenus.update(mid, {title: prefix + " " + request.target});
  }
  else
  {
    chrome.contextMenus.update(mid, {title: prefix + " [n/a]"});
  }
  
  if(request.stack && request.stack.length > 0)
  {
    chrome.contextMenus.update(bid, {title: cmdBlock + " [" + request.stack.length + "]"});
    
    removeOldUrls();
    if(debugLog) console.log(request.stack);
    var n = request.stack.length;
    for(var i = 0; i < n; i++)
    {
      ready2blockIds.push(
        createMenuItem(
        {
          contexts: ['all'], // ['image', 'link' , 'frame'],
          title: request.stack[i],
          parentId: bid,
          type: 'checkbox',
          checked: ifExist(request.stack[i])
        },
        function(info, tab, init)
        {
          if(debugLog) console.log('clicked:' + info.menuItemId + " " + init.title);
          if(init.checked)
          {
            var x = urlOffset(init.title);
            if(confirm("Delete existing record '" + blackList[x] + "'?"))
            {
              blackList.splice(x, 1);
              setData("blackList", blackList.join('\t'));
              return;
            }
          }
          
          var text = prompt("Please edit URL to block in future", init.title);
          if(text != null)
          {
            mergeBlackList(text);
          }
          // log('link:' + info.linkUrl + " src:" + info.srcUrl + " frame:" + info.frameUrl);
        },
        function()
        {
          // log("created:" + ready2blockIds);
        })
      );
    }
  }
  else
  {
    removeOldUrls();
    chrome.contextMenus.update(bid, {title: cmdBlock + " [n/a]"}); // type: "separator"
  }
});

restoreBlackList();
